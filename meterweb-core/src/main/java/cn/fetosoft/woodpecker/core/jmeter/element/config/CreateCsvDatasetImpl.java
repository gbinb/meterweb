package cn.fetosoft.woodpecker.core.jmeter.element.config;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.config.CsvDataSetElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建CSV数据设置组件
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/22 13:57
 */
@Component("csvDataSetImpl")
public class CreateCsvDatasetImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		CsvDataSetElement element = (CsvDataSetElement) be;
		CsvDataSetExt dataSet = new CsvDataSetExt();
		dataSet.setFilename(element.getFilePath());
		dataSet.setFileEncoding(element.getFileEncoding());
		dataSet.setVariableNames(element.getVariableNames());
		if(StringUtils.isNotBlank(element.getIgnoreFirstLine())){
			dataSet.setIgnoreFirstLine(Boolean.parseBoolean(element.getIgnoreFirstLine()));
		}else{
			dataSet.setIgnoreFirstLine(false);
		}
		dataSet.setDelimiter(element.getDelimiter());
		if(StringUtils.isNotBlank(element.getQuotedData())){
			dataSet.setQuotedData(Boolean.parseBoolean(element.getQuotedData()));
		}else{
			dataSet.setQuotedData(false);
		}
		if(StringUtils.isNotBlank(element.getRecycle())){
			dataSet.setRecycle(Boolean.parseBoolean(element.getRecycle()));
		}else{
			dataSet.setRecycle(false);
		}
		dataSet.setShareMode(element.getShareMode());
		if(StringUtils.isNotBlank(element.getStopThread())){
			dataSet.setStopThread(Boolean.parseBoolean(element.getStopThread()));
		}else {
			dataSet.setStopThread(false);
		}
		dataSet.initProperties();
		return dataSet;
	}
}
