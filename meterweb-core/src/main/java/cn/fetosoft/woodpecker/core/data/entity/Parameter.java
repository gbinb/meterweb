package cn.fetosoft.woodpecker.core.data.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/10 17:44
 */
@Setter
@Getter
public class Parameter {

	private String name;

	private String value;

	public Parameter(String name, String value){
		this.name = name;
		this.value = value;
	}
}
