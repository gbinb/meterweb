package cn.fetosoft.woodpecker.core.data.entity.element.config;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.MultiArguments;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.List;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/15 9:36
 */
@Setter
@Getter
@Document("wp_element")
public class HttpHeaderElement extends BaseElement implements MultiArguments {

	@DataField
	private String[] names;

	@DataField
	private String[] values;

	@Override
	public List<String> getFields() {
		return Arrays.asList("names", "values");
	}
}
