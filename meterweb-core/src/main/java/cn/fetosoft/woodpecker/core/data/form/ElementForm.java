package cn.fetosoft.woodpecker.core.data.form;

import cn.fetosoft.woodpecker.core.data.base.BaseForm;
import cn.fetosoft.woodpecker.core.data.base.Condition;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.FieldType;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/11 9:16
 */
@Setter
@Getter
@Document("wp_element")
public class ElementForm extends BaseForm {

	@Condition(fieldType= FieldType.OBJECT_ID)
	private String id;

	@Condition
	private String elementId;

	@Condition
	private String planId;

	@Condition
	private String category;

	@Condition
	private String parentId;
}
