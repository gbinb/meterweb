package cn.fetosoft.woodpecker.core.data.entity.element.processor;

import cn.fetosoft.woodpecker.core.data.entity.element.sampler.JdbcRequestElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * jdbc前置处理器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/6 21:05
 */
@Setter
@Getter
@Document("wp_element")
public class JdbcPreElement extends JdbcRequestElement {
}
