package cn.fetosoft.woodpecker.core.pubsub.zookeeper;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 11:16
 */
public class CuratorFrameworkBuilder {

    private ZookeeperCfg cfg;

    public CuratorFrameworkBuilder(ZookeeperCfg cfg){
        this.cfg = cfg;
    }

    public CuratorFramework createFramework(){
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(cfg.getRetryInterval(), cfg.getRetries());
        CuratorFramework client = CuratorFrameworkFactory.newClient(cfg.getHosts(), retryPolicy);
        client.start();
        return client;
    }
}
