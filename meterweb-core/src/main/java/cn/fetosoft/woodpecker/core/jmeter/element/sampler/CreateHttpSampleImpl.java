package cn.fetosoft.woodpecker.core.jmeter.element.sampler;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.sampler.HttpSamplerElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.protocol.http.control.Header;
import org.apache.jmeter.protocol.http.control.HeaderManager;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerProxy;
import org.apache.jmeter.protocol.http.sampler.HttpRequestSampler;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建HTTPSamplerProxy
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 9:57
 */
@Component("httpSamplerImpl")
public class CreateHttpSampleImpl extends AbstractElementBuild {

	private static final String CONTENT_JSON = "json";
	private static final String CONTENT_FORM = "form";

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		HttpRequestSampler proxy = new HttpRequestSampler();
		HttpSamplerElement element = (HttpSamplerElement) be;
		proxy.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());

		if(StringUtils.isNotBlank(element.getProtocol())){
			proxy.setProtocol(element.getProtocol());
		}
		if(StringUtils.isNotBlank(element.getDomain())){
			proxy.setDomain(element.getDomain());
		}
		if(StringUtils.isNotBlank(element.getPort())) {
			proxy.setPort(Integer.parseInt(element.getPort()));
		}
		if(StringUtils.isNotBlank(element.getConnectTimeout())){
			proxy.setConnectTimeout(element.getConnectTimeout());
		}
		if(StringUtils.isNotBlank(element.getResponseTimeout())){
			proxy.setResponseTimeout(element.getResponseTimeout());
		}
		if(StringUtils.isNotBlank(element.getContentEncode())){
			proxy.setContentEncoding(element.getContentEncode());
		}
		if(StringUtils.isNotBlank(element.getPath())){
			proxy.setPath(element.getPath());
		}
		proxy.setMethod(element.getMethod());
		proxy.setAutoRedirects(element.isAutoRedirects());
		proxy.setFollowRedirects(element.isFollowRedirects());
		proxy.setUseKeepAlive(element.isUseKeepalive());
		if(CONTENT_JSON.equalsIgnoreCase(element.getPostBodyRaw())){
			proxy.setPostBodyRaw(true);
		}else{
			proxy.setPostBodyRaw(false);
		}
		HeaderManager headerManager = new HeaderManager();
		if(proxy.getPostBodyRaw()){
			headerManager.add(new Header("Content-Type", "application/json"));
			proxy.setDoMultipart(false);
			proxy.addNonEncodedArgument("", element.getArguments(), "=");
		}else{
			if(CONTENT_FORM.equalsIgnoreCase(element.getPostBodyRaw())){
				headerManager.add(new Header("Content-Type", "application/x-www-form-urlencoded"));
				proxy.setDoMultipart(false);
			}else{
				headerManager.add(new Header("Content-Type", "multipart/form-data"));
				proxy.setDoMultipart(true);
			}
			String[] names = element.getNames();
			String[] values = element.getValues();
			for(int i=0; i<names.length; i++){
				proxy.addEncodedArgument(names[i], values[i], "=");
			}
		}
		proxy.setHeaderManager(headerManager);
		return proxy;
	}
}
