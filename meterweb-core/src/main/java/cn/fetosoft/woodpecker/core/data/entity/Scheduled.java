package cn.fetosoft.woodpecker.core.data.entity;

import cn.fetosoft.woodpecker.core.data.base.BaseEntity;
import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.enums.DateFormatEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 定时任务
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/27 15:58
 */
@Setter
@Getter
@Document("wp_scheduled")
public class Scheduled extends BaseEntity {

	@DataField
	private String name;

	@DataField
	private String description;

	private String jobClass;

	@DataField
	private String expression;

	private String host;

	@DataField
	private Integer status;

	private String statusText;

	private String createTimeFmt;

	@DataField
	private Long startTime;

	private String startTimeFmt;

	@DataField
	private Long stopTime;

	private String stopTimeFmt;

	private String userId;

	@DataField
	private String planId;

	private String username;

	private String planName;

	@DataField
	private String errorMsg;

	public String getCreateTimeFmt() {
		if(this.getCreateTime()!=null){
			createTimeFmt = DateFormatEnum.YMD_HMS.timestampToString(this.getCreateTime());
		}
		return createTimeFmt;
	}

	public String getStartTimeFmt() {
		if(this.getStartTime()!=null){
			startTimeFmt = DateFormatEnum.YMD_HMS.timestampToString(this.getStartTime());
		}
		return startTimeFmt;
	}

	public String getStopTimeFmt() {
		if(this.getStopTime()!=null){
			stopTimeFmt = DateFormatEnum.YMD_HMS.timestampToString(this.getStopTime());
		}
		return stopTimeFmt;
	}

	public String getStatusText() {
		if(this.getStatus()!=null){
			statusText = this.getStatus()==1?"执行中":"停止";
		}else{
			statusText = "未知";
		}
		return statusText;
	}
}
