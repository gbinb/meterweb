package cn.fetosoft.woodpecker.core.util;

/**
 * 常量
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/8/4 21:46
 */
public class Constant {

    /**
     * 根节点的父ID
     */
    public static final String ROOT_PARENT_ID = "0000";

    public static final String EID = "eid";
    public static final String PLAN_ID = "planId";
    public static final String ELEMENT_ID = "elementId";
    public static final String PARENT_ID = "parentId";
    public static final String CATEGORY = "category";
    public static final String USER_ID = "userId";
    public static final String TEST_ID = "testId";

    public static final String PATH_CONFIG = "config";
    public static final String PATH_SAMPLER = "sampler";
    public static final String PATH_PROCESSOR = "processor";
    public static final String PATH_ASSERTION = "assertion";
    public static final String PATH_LISTENER = "listener";
    public static final String PATH_CONTROLLER = "controller";
}
