package cn.fetosoft.woodpecker.core.event;

import cn.fetosoft.woodpecker.core.data.entity.TestResult;
import org.springframework.context.ApplicationEvent;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/24 9:12
 */
public class SampleResultEvent extends ApplicationEvent {

	private String userId;

	/**
	 *
	 */
	public SampleResultEvent(TestResult source) {
		super(source);
	}

	public TestResult getTestResult(){
		return (TestResult) getSource();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
