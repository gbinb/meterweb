package cn.fetosoft.woodpecker.core.jmeter;

/**
 * Callback after the collector ends
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/1 14:22
 */
public interface CollectorCallback<T> {

	/**
	 * callback
	 * @author guobingbing
	 * @wechat t_gbinb
	 * @date 2021/7/1 14:25
	 * @param t
	 * @return void
	 * @version 1.0
	 */
	void call(T t);
}
