package cn.fetosoft.woodpecker.core.enums;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.TestPlanElement;
import cn.fetosoft.woodpecker.core.data.entity.element.*;
import cn.fetosoft.woodpecker.core.data.entity.element.assertion.JsonPathAssertionElement;
import cn.fetosoft.woodpecker.core.data.entity.element.assertion.ResponseAssertionElement;
import cn.fetosoft.woodpecker.core.data.entity.element.config.*;
import cn.fetosoft.woodpecker.core.data.entity.element.controller.*;
import cn.fetosoft.woodpecker.core.data.entity.element.listener.AggregateReportElement;
import cn.fetosoft.woodpecker.core.data.entity.element.listener.ViewResultTreeElement;
import cn.fetosoft.woodpecker.core.data.entity.element.processor.*;
import cn.fetosoft.woodpecker.core.data.entity.element.sampler.BeanShellSamplerElement;
import cn.fetosoft.woodpecker.core.data.entity.element.sampler.DebugSamplerElement;
import cn.fetosoft.woodpecker.core.data.entity.element.sampler.HttpSamplerElement;
import cn.fetosoft.woodpecker.core.data.entity.element.sampler.JdbcRequestElement;
import static cn.fetosoft.woodpecker.core.util.Constant.*;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/17 13:25
 */
public enum ElementCategory {

	/**
	 * 测试计划
	 */
	TEST_PLAN("testPlan", "测试计划"){
		@Override
		public Class<TestPlanElement> getElementClass() {
			return TestPlanElement.class;
		}

		@Override
		public String getElementPage() {
			return this.getName();
		}
	},

	THREAD_GROUP("threadGroup", "线程组"){
		@Override
		public Class<ThreadGroupElement> getElementClass() {
			return ThreadGroupElement.class;
		}

		@Override
		public String getElementPage() {
			return this.getName();
		}
	},

	HTTP_CONFIG("httpConfig", "HTTP默认配置"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return HttpConfigElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_CONFIG + "/" + this.getName();
		}
	},

	HTTP_HEADER("httpHeader", "HTTP消息头"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return HttpHeaderElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_CONFIG + "/" + this.getName();
		}
	},

	DATA_SOURCE_CONFIG("dataSourceConfig", "JDBC数据源配置"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return DataSourceConfigElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_CONFIG + "/" + this.getName();
		}
	},

	HTTP_SAMPLER("httpSampler", "HTTP请求"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return HttpSamplerElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_SAMPLER + "/" + this.getName();
		}
	},

	JDBC_REQUEST("jdbcRequest", "JDBC Request"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return JdbcRequestElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_SAMPLER + "/" + this.getName();
		}
	},

	DEBUG_SAMPLER("debugSampler", "Debug取样器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return DebugSamplerElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_SAMPLER + "/" + this.getName();
		}
	},

	BEAN_SHELL_SAMPLER("beanShellSampler", "BeanShell取样器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return BeanShellSamplerElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_SAMPLER + "/" + this.getName();
		}
	},

	PARAMS_SIGN("paramsSign", "HTTP参数签名"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return ParamsSignElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_PROCESSOR + "/" + this.getName();
		}
	},

	ID_GENERATOR("idGenerator", "随机ID生成器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return IdGeneratorElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_CONFIG + "/" + this.getName();
		}
	},

	VIEW_RESULT_TREE("viewResultTree", "查看结果树"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return ViewResultTreeElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_LISTENER + "/" + this.getName();
		}
	},

	AGGREGATE_REPORT("aggregateReport", "聚合报告"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return AggregateReportElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_LISTENER + "/" + this.getName();
		}
	},

	JSON_EXTRACT("jsonExtract", "JSON提取器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return JsonExtractElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_PROCESSOR + "/" + this.getName();
		}
	},

	REGEX_EXTRACTOR("regexExtractor", "正则表达式提取器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return RegexExtractorElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_PROCESSOR + "/" + this.getName();
		}
	},

	CSV_DATASET("csvDataSet", "CSV数据文件设置"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return CsvDataSetElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_CONFIG + "/" + this.getName();
		}
	},

	DEBUG_POST("debugPost", "调试后置处理器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return DebugPostElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_PROCESSOR + "/" + this.getName();
		}
	},

	AES_SECRET("aesSecret", "AES加解密处理器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return AesSecretElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_PROCESSOR + "/" + this.getName();
		}
	},

	JDBC_PRE("jdbcPre", "JDBC前置处理器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return JdbcPreElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_PROCESSOR + "/" + this.getName();
		}
	},

	JDBC_POST("jdbcPost", "JDBC后置处理器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return JdbcPostElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_PROCESSOR + "/" + this.getName();
		}
	},

	BEAN_SHELL_PRE("beanShellPre", "BeanShell前置处理器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return BeanShellPreElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_PROCESSOR + "/" + this.getName();
		}
	},

	BEAN_SHELL_POST("beanShellPost", "BeanShell后置处理器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return BeanShellPostElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_PROCESSOR + "/" + this.getName();
		}
	},

	RESULT_ACTION("resultAction", "结果状态处理器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return ResultActionElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_PROCESSOR + "/" + this.getName();
		}
	},

	CRITICAL_SECTION("criticalSection", "临界部分控制器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return CriticalSectionElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_CONTROLLER + "/" + this.getName();
		}
	},

	IF_CONTROLLER("ifController", "IF控制器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return IfControllerElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_CONTROLLER + "/" + this.getName();
		}
	},

	MODULE_CONTROLLER("moduleController", "模块控制器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return ModuleControllerElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_CONTROLLER + "/" + this.getName();
		}
	},

	LOOP_CONTROLLER("loopController", "循环控制器"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return LoopControllerElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_CONTROLLER + "/" + this.getName();
		}
	},

	TEST_FRAGMENT("testFragment", "测试片段"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return TestFragmentElement.class;
		}

		@Override
		public String getElementPage() {
			return this.getName();
		}
	},

	RESPONSE_ASSERTION("responseAssertion", "响应断言"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return ResponseAssertionElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_ASSERTION + "/" + this.getName();
		}
	},

	JSON_PATH_ASSERTION("jsonPathAssertion", "JSON断言"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return JsonPathAssertionElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_ASSERTION + "/" + this.getName();
		}
	},

	USER_ARGUMENT("userArgument", "用户定义的变量"){
		@Override
		public Class<? extends BaseElement> getElementClass() {
			return UserArgumentElement.class;
		}

		@Override
		public String getElementPage() {
			return PATH_CONFIG + "/" + this.getName();
		}
	},
	;

	private String name;
	private String text;

	ElementCategory(String name, String text){
		this.name = name;
		this.text = text;
	}

	public String getName(){
		return this.name;
	}

	public String getText(){
		return this.text;
	}

	public static ElementCategory getCategory(String name){
		ElementCategory[] categories = ElementCategory.values();
		for(int i=0; i<categories.length; i++){
			if(categories[i].getName().equals(name)){
				return categories[i];
			}
		}
		return null;
	}

	public abstract Class<? extends  BaseElement> getElementClass();

	public abstract String getElementPage();
}
