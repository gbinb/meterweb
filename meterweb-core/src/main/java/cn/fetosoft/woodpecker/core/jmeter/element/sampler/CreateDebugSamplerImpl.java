package cn.fetosoft.woodpecker.core.jmeter.element.sampler;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.sampler.DebugSamplerElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.sampler.DebugSampler;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建Debug取样器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/10 17:50
 */
@Component("debugSamplerImpl")
public class CreateDebugSamplerImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		DebugSamplerElement element = (DebugSamplerElement) be;
		DebugSampler sampler = new DebugSampler();
		String properties = StringUtils.isNotBlank(element.getDisplayJMeterProperties())?element.getDisplayJMeterProperties():"false";
		sampler.setProperty("displayJMeterProperties", Boolean.parseBoolean(properties));
		String variables = StringUtils.isNotBlank(element.getDisplayJMeterVariables())?element.getDisplayJMeterVariables():"false";
		sampler.setProperty("displayJMeterVariables", Boolean.parseBoolean(variables));
		String systemProps = StringUtils.isNotBlank(element.getDisplaySystemProperties())?element.getDisplaySystemProperties():"false";
		sampler.setProperty("displaySystemProperties", Boolean.parseBoolean(systemProps));
		return sampler;
	}
}
