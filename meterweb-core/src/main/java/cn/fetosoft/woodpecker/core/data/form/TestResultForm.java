package cn.fetosoft.woodpecker.core.data.form;

import cn.fetosoft.woodpecker.core.data.base.BaseForm;
import cn.fetosoft.woodpecker.core.data.base.Condition;
import cn.fetosoft.woodpecker.core.data.base.ConditionType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.util.Date;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/24 11:14
 */
@Setter
@Getter
@Document("wp_result")
public class TestResultForm extends BaseForm {

	@Condition(fieldType= FieldType.OBJECT_ID)
	private String id;
	@Condition
	private String testId;
	@Condition
	private String userId;
	@Condition
	private String elementId;
	@Condition
	private String planId;
	@Condition
	private String category;
	@Condition
	private String parentId;
	@Condition
	private String threadId;
	@Condition
	private String name;
	@Condition
	private String ip;

	private String testDate;

	@Condition(fieldType = FieldType.DATE_TIME, fieldName = "createTime", conditionType = ConditionType.GTE)
	private Date startTime;

	@Condition(fieldType = FieldType.DATE_TIME, fieldName = "createTime", conditionType = ConditionType.LTE)
	private Date endTime;
}
