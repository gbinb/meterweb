package cn.fetosoft.woodpecker.core.jmeter;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/23 14:45
 */
public interface JMeterService {

	/**
	 * 启动测试
	 * @author guobingbing
	 * @wechat t_gbinb 
	 * @date 2021/6/24 11:25
	 * @param userId 用户ID
	 * @param testId 测试ID
	 * @param planId 计划ID
	 * @return void	
	 * @version 1.0	
	 */	
	void startTest(String userId, String testId, String planId) throws Exception;
}
