package cn.fetosoft.woodpecker.core.data.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/23 9:35
 */
@Setter
@Getter
public class BaseCollector extends BaseElement {

	private boolean errorLogging;

	private String filename;
}
