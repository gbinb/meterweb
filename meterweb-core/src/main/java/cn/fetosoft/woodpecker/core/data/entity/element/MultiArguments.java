package cn.fetosoft.woodpecker.core.data.entity.element;

import java.util.List;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/21 10:50
 */
public interface MultiArguments {

	/**
	 * 获取字段数组
	 * @return
	 */
	List<String> getFields();
}
