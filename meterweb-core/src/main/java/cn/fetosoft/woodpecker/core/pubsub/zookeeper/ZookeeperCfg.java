package cn.fetosoft.woodpecker.core.pubsub.zookeeper;

import lombok.Getter;
import lombok.Setter;

/**
 * zookeeper配置
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 11:16
 */
@Setter
@Getter
public class ZookeeperCfg {

    /**
     * 主机地址
     */
    private String hosts;

    /**
     * 重试次数
     */
    private int retries;

    /**
     * 重试间隔时间
     */
    private int retryInterval;

    /**
     * 订阅path
     */
    private String subPath;

    /**
     * 注册path
     */
    private String regPath;
}
