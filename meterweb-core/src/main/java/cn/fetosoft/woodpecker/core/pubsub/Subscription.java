package cn.fetosoft.woodpecker.core.pubsub;

import cn.fetosoft.woodpecker.core.pubsub.zookeeper.NodeEntity;

import java.util.List;

/**
 * 订阅数据
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 11:05
 */
public interface Subscription {

    /**
     * 订阅数据
     * @throws Exception
     */
    void subscript() throws Exception;

    /**
     * 查询所有注册的节点
     * @return
     * @throws Exception
     */
    List<NodeEntity> getRegisterClusters() throws Exception;
}
