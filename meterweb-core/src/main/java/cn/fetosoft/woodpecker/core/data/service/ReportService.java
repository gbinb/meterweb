package cn.fetosoft.woodpecker.core.data.service;

import cn.fetosoft.woodpecker.core.data.base.BaseDataService;
import cn.fetosoft.woodpecker.core.data.entity.Report;
import cn.fetosoft.woodpecker.core.data.form.ReportForm;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/1 16:07
 */
public interface ReportService extends BaseDataService<Report, ReportForm> {

	/**
	 * 删除聚合报表数据，同时删除相关sample数据
	 * @param id
	 * @return
	 */
	long deleteAggregateById(String id) throws Exception;
}
