package cn.fetosoft.woodpecker.core.data.entity.element.config;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 数据源配置
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/8/1 20:57
 */
@Setter
@Getter
@Document("wp_element")
public class DataSourceConfigElement extends BaseElement {

    @DataField
    private String dataSource;
    @DataField
    private String poolMax;
    @DataField
    private String timeout;
    @DataField
    private String trimInterval;
    @DataField
    private String autocommit;
    @DataField
    private String transactionIsolation;
    @DataField
    private String preinit;
    @DataField
    private String initQuery;
    @DataField
    private String keepAlive;
    @DataField
    private String connectionAge;
    @DataField
    private String checkQuery;
    @DataField
    private String dbUrl;
    @DataField
    private String driver;
    @DataField
    private String username;
    @DataField
    private String password;
    @DataField
    private String connectionProperties;
}
