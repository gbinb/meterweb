package cn.fetosoft.woodpecker.core.data.entity.element.processor;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 参数签名处理器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/8/8 16:26
 */
@Setter
@Getter
@Document("wp_element")
public class ParamsSignElement extends BaseElement {

	@DataField
	private String signName;
	@DataField
	private String signType;
	@DataField
	private String signKey;
	@DataField
	private String paramName;
	@DataField
	private String jsonPath;
	@DataField
	private String signJoiner;
}
