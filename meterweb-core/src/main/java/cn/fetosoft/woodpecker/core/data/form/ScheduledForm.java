package cn.fetosoft.woodpecker.core.data.form;

import cn.fetosoft.woodpecker.core.data.base.BaseForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/27 16:20
 */
@Setter
@Getter
@Document("wp_scheduled")
public class ScheduledForm extends BaseForm {

	private String name;

	private Integer status;

	private String userId;

	private String planId;
}
