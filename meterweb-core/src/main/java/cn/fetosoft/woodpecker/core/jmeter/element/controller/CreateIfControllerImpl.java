package cn.fetosoft.woodpecker.core.jmeter.element.controller;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.controller.IfControllerElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.control.IfController;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建IF控制器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/12 21:03
 */
@Component("ifControllerImpl")
public class CreateIfControllerImpl extends AbstractElementBuild {

    @Override
    protected TestElement convert(BaseElement be) throws Exception {
        IfControllerElement element = (IfControllerElement) be;
        IfController controller = new IfController();
        controller.setCondition(element.getCondition());
        if(StringUtils.isNotBlank(element.getEvaluateAll())){
            controller.setEvaluateAll(Boolean.parseBoolean(element.getEvaluateAll()));
        }
        if(StringUtils.isNotBlank(element.getUseExpression())){
            controller.setUseExpression(Boolean.parseBoolean(element.getUseExpression()));
        }
        return controller;
    }
}
