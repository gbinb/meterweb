package cn.fetosoft.woodpecker.struct;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/14 22:02
 */
public class DataGrid<T> {

    private long total;
    private List<T> rows = new ArrayList<>();

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows.addAll(rows);
    }

    public String toJsonString(){
        return JSON.toJSONString(this);
    }
}
