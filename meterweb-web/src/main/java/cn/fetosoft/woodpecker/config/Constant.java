package cn.fetosoft.woodpecker.config;

/**
 * 定义的常量
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/16 21:19
 */
public final class Constant {

    public static final String SESSION_USER = "session_user";

    public static final String HMAC_SHA1_KEY = "Tple90Ldaxlo948d";
}
