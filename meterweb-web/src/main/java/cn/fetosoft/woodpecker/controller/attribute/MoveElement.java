package cn.fetosoft.woodpecker.controller.attribute;

import lombok.Getter;
import lombok.Setter;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/26 14:58
 */
@Setter
@Getter
public class MoveElement {

	private String id;

	private int sort;

	private String moveId;

	private int moveSort;

	private boolean enabled;

	private String category;

	private String parentId;

	private String planId;
}
