/**
 * 字符串连接
 * 用法：var sb = new StringBuilder();
 *      sb.append('');
 * @constructor
 */
function StringBuilder(){
    this.data = [];
}

StringBuilder.prototype.append = function(){
    this.data.push(arguments[0]);
    return this;
};

StringBuilder.prototype.toString = function(){
    return this.data.join("");
};

/**
 * 表单转json
 * author guobingbing
 * description 使用方法 var jsonObj = $('#form').serializeJSON();
 * @returns {{}}
 */
$.fn.serializeJSON = function(){
    let o = {};
    let a = this.serializeArray();
    let notChecked=$('input[type=checkbox]:not(:checked)', this);
    $.each(notChecked, function () {
        if (!a.hasOwnProperty(this.name)){
            a.push({name: this.name, value: ""});
        }
    });
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

/**
 * Post json数据
 * @param url
 * @param data
 * @param callbackFun
 */
function postJson(url, data, callbackFun) {
    $.ajax({
        type: 'POST',
        url: url,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        dataType: "json",
        success: function (result) {
            callbackFun(true, result);
        },
        error: function (result) {
            callbackFun(false, result);
        }
    });
}

/**
 * Get请求
 * @param url
 * @param data
 * @param callbackFun
 */
function httpGet(url, data, callbackFun) {
    $.ajax({
        type: 'POST',
        url: url,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        dataType: "json",
        success: function (result) {
            callbackFun(true, result);
        },
        error: function (result) {
            callbackFun(false, result);
        }
    });
}

/**
 * 确认框
 * @param title
 * @param callbackFunc
 * @returns {*}
 */
function layerConfirm(title, callbackFunc) {
    let lay = layer.confirm(title, {
        btn: ['确定','取消'] //按钮
    },function () {
        layer.close(lay);
        callbackFunc(true);
    },function () {
        callbackFunc(false);
    });
    return lay;
}

/**
 * 加载框：0-红色圆球，1-灯
 * @param style
 * @returns {*}
 */
function layerLoading(style) {
    if(!style){
        style = 1;
    }
    return layer.load(style,{shade: [0.3,'#000']});
}

/**
 * 右下角滑出
 * @param content
 */
function layerSlide(content) {
    layer.open({
        type: 1,
        title: '提示',
        closeBtn: 1, //不显示关闭按钮
        shade: 0,
        area: ['280px', '150px'],
        offset: 'rb', //右下角弹出
        time: 1800, //2秒后自动关闭
        anim: 2,
        content: "<p style='padding: 10px;'>" + content + "</p>"
    });
}

/**
 * 阻塞执行
 * @param delay
 */
function sleep(delay) {
    for(let t = Date.now(); Date.now() - t <= delay;);
}