<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div id="dlg_scheduled" class="easyui-window" buttons="#dlg_scheduled_buttons"
     data-options="modal:true,closed:true,width:560,inline:true,border:'thin',cls:'c6'">
    <div style="padding: 20px 30px">
        <form id="form_scheduled" method="post">
            <input id="form_scheduled_id" type="hidden" name="id">
            <div class="form_item_height">
                <input class="easyui-textbox" name="name" style="width:90%"
                       data-options="label:'名称：',labelWidth:100,required:true">
            </div>
            <div class="form_item_height">
                <input class="easyui-textbox" name="expression" style="width:90%"
                       data-options="label:'Cron表达式：',labelWidth:100,required:true">
            </div>
            <div class="form_item_height">
                <input class="easyui-combobox" name="planId" style="width:90%;" data-options="
                    url: '/auth/element/getPlansDict',
                    method:'get',
                    editable: false,
                    required: true,
                    valueField: 'id',
                    textField: 'text',
                    panelMaxHeight: 180,
                    label: '测试计划：',
                    labelWidth:100
                    ">
            </div>
            <div class="form_item_height">
                <input class="easyui-textbox" name="description" style="width:90%"
                       data-options="label:'备注：',labelWidth:100">
            </div>
        </form>
    </div>
</div>
<div id="dlg_scheduled_buttons">
    <a href="javascript:void(0)" onclick="saveScheduled()" class="btn btn-primary">
        <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M15.854 5.646a.5.5 0 010 .708l-7 7a.5.5 0 01-.708 0l-3.5-3.5a.5.5 0 11.708-.708L8.5 12.293l6.646-6.647a.5.5 0 01.708 0z" clip-rule="evenodd"></path>
        </svg>
        保存
    </a>
    <a href="javascript:void(0)" onclick="$('#dlg_scheduled').dialog('close')" class="btn btn-secondary">
        <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M7.578 6.437a5 5 0 104.922.044l.5-.865a6 6 0 11-5.908-.053l.486.874z" clip-rule="evenodd"></path>
            <path fill-rule="evenodd" d="M9.5 10V3h1v7h-1z" clip-rule="evenodd"></path>
        </svg>
        取消
    </a>
</div>