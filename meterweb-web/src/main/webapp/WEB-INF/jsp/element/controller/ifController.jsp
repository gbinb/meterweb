<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">IF控制器</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}', 'ifController_form_', function(eid, status) {
                            if(status===1){
                            menuTree.updateNodeText($('#ifController_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="ifController_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="name" id="ifController_form_name_${eid}" style="width:90%" data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:90%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div>
                        <input class="easyui-textbox" name="condition" style="width:90%;height: 200px;" multiline="true"
                               data-options="label:'条件表达式:',labelWidth:150,required:true">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <input class="easyui-checkbox" name="useExpression" value="true"
                               data-options="label:'将条件解释为变量表达式：',labelWidth:250">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-checkbox" name="evaluateAll" value="true"
                               data-options="label:'应用于所有子流程：',labelWidth:250">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <p>
                            表达式的使用：假设上下文中有变量“code” ，期望值为“200”则执行，则表达式写为 "&#36{code}"=="200"，英文双引号不能少；
                        </p>
                        <p>
                            <a href="javascript:void(0)">&#36{JMeterThread.last_sample_ok}</a> can be used to test if last sampler was successful.
                        </p>
                        <p>
                            变量表达式是指使用“__jex13”或“__groovy”格式表达式计算变量值，
                            <a href="https://www.blazemeter.com/blog/six-tips-for-jmeter-if-controller-usage" target="_blank">查看使用示例</a>，
                            <a href="https://jmeter.apache.org/usermanual/component_reference.html#If_Controller" target="_blank">官方帮助文档</a>；
                        </p>
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}', 'ifController_form_', function(eid, status) {
                    if(status===1){
                    menuTree.updateNodeText($('#ifController_form_name_' + eid).val());
                    }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>