<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">响应断言</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','responseAssertion_form_',function(eid,status) {
                            if(status===1){
                            menuTree.updateNodeText($('#responseAssertion_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="responseAssertion_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="name" id="responseAssertion_form_name_${eid}" style="width:90%" data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:90%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <div class="row">
                            <div class="col-2">应用范围：</div>
                            <div class="col-10">
                                <input class="easyui-radiobutton" name="scope" value="all"
                                       data-options="label:'主样本及子样本',labelPosition:'after',labelWidth:120">
                                <input class="easyui-radiobutton" name="scope" value="parent"
                                       data-options="label:'仅主样本',labelPosition:'after',labelWidth:120,checked:true">
                                <input class="easyui-radiobutton" name="scope" value="children"
                                       data-options="label:'仅子样本',labelPosition:'after',labelWidth:120">
                                <input class="easyui-radiobutton" name="scope" value="variable"
                                       data-options="label:'使用JMeter变量',labelPosition:'after',labelWidth:120">
                            </div>
                        </div>
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="scopeVariable" style="width:90%" data-options="label:'JMeter变量名：',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <div class="row">
                            <div class="col-2">测试字段：</div>
                            <div class="col-10">
                                <input class="easyui-radiobutton" name="testField" value="Assertion.response_data"
                                       data-options="label:'响应文本',labelPosition:'after',labelWidth:100,checked:true">
                                <input class="easyui-radiobutton" name="testField" value="Assertion.response_code"
                                       data-options="label:'响应代码',labelPosition:'after',labelWidth:100">
                                <input class="easyui-radiobutton" name="testField" value="Assertion.response_message"
                                       data-options="label:'响应信息',labelPosition:'after',labelWidth:100">
                                <input class="easyui-radiobutton" name="testField" value="Assertion.response_headers"
                                       data-options="label:'响应头',labelPosition:'after',labelWidth:100">
                                <input class="easyui-radiobutton" name="testField" value="Assertion.request_headers"
                                       data-options="label:'请求头',labelPosition:'after',labelWidth:100">
                            </div>
                        </div>
                    </div>
                    <div class="form_item_height">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-10">
                                <input class="easyui-radiobutton" name="testField" value="Assertion.sample_label"
                                       data-options="label:'URL样本',labelPosition:'after',labelWidth:100">
                                <input class="easyui-radiobutton" name="testField" value="Assertion.response_data_as_document"
                                       data-options="label:'文档文本',labelPosition:'after',labelWidth:100">
                                <input class="easyui-radiobutton" name="testField" value="Assertion.request_data"
                                       data-options="label:'请求数据',labelPosition:'after',labelWidth:100">
                                <input class="easyui-checkbox" name="assumeSuccess" value="true"
                                       data-options="label:'忽略状态',labelPosition:'after',labelWidth:100">
                            </div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <div class="row">
                            <div class="col-2">模式匹配规则：</div>
                            <div class="col-10">
                                <input class="easyui-radiobutton" name="testRule" value="2"
                                       data-options="label:'包括',labelPosition:'after',labelWidth:100">
                                <input class="easyui-radiobutton" name="testRule" value="1"
                                       data-options="label:'匹配',labelPosition:'after',labelWidth:100">
                                <input class="easyui-radiobutton" name="testRule" value="8"
                                       data-options="label:'相等',labelPosition:'after',labelWidth:100">
                                <input class="easyui-radiobutton" name="testRule" value="16"
                                       data-options="label:'字符串',labelPosition:'after',labelWidth:100,checked:true">
                            </div>
                        </div>
                    </div>
                    <div class="form_item_height">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-10">
                                <input class="easyui-checkbox" name="testNo" value="4"
                                       data-options="label:'否',labelPosition:'after',labelWidth:100">
                                <input class="easyui-checkbox" name="testOr" value="32"
                                       data-options="label:'或者',labelPosition:'after',labelWidth:100">
                            </div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <p>
                            多条测试模式以英文“;”分隔
                        </p>
                        <div>
                            <input class="easyui-textbox" name="testStrings" style="width:90%"
                                   data-options="label:'测试模式：',labelWidth:150,multiline:true,height:100">
                        </div>
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <div>
                            <input class="easyui-textbox" name="customMessage" style="width:90%"
                                   data-options="label:'自定义失败消息：',labelWidth:150,multiline:true,height:100">
                        </div>
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','responseAssertion_form_',function(eid,status) {
                    if(status===1){
                    menuTree.updateNodeText($('#responseAssertion_form_name_' + eid).val());
                    }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>