<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">JDBC后置处理器</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}', 'jdbcPost_form_', function(eid, status) {
                            if(status===1){
                            menuTree.updateNodeText($('#jdbcPost_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="jdbcPost_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="name" id="jdbcPost_form_name_${eid}" style="width:90%" data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:90%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="dataSource" style="width:90%" data-options="label:'JDBC连接数据源：',labelWidth:150,required:true">
                    </div>
                    <div class="form_item_height">
                        <select class="easyui-combobox" name="queryType" style="width:90%;" data-options="label:'SQL语句类型：',labelWidth:150,required:true,editable:false">
                            <option value="Select Statement">Select Statement</option>
                            <option value="Update Statement">Update Statement</option>
                            <option value="Callable Statement">Callable Statement</option>
                            <option value="Prepared Select Statement">Prepared Select Statement</option>
                            <option value="Prepared Update Statement">Prepared Update Statement</option>
                            <option value="Commit">Commit</option>
                            <option value="Rollback">Rollback</option>
                            <option value="AutoCommit(false)">AutoCommit(false)</option>
                            <option value="AutoCommit(true)">AutoCommit(true)</option>
                        </select>
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="query" style="width:90%"
                               data-options="label:'SQL语句：',labelWidth:150,multiline:true,height:150,required:true">
                    </div>
                    <div class="form_item_height">
                        <p style="padding-left: 150px;">多个参数之间使用英文“,”隔开，下同</p>
                        <input class="easyui-textbox" name="queryArguments" style="width:90%" data-options="label:'SQL参数值：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="queryArgumentsTypes" style="width:90%" data-options="label:'SQL参数类型：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="variableNames" style="width:90%" data-options="label:'变量名称：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="resultVariable" style="width:90%" data-options="label:'执行结果变量名：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="queryTimeout" style="width:90%" data-options="label:'查询超时(s)：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="resultSetMaxRows" style="width:90%" data-options="label:'最大查询行数：',labelWidth:150">
                    </div>
                    <div>
                        <select class="easyui-combobox" name="resultSetHandler" style="width:90%;" data-options="label:'结果集处理：',labelWidth:150,editable:false">
                            <option value="Store as String">Store as String</option>
                            <option value="Store as Object">Store as Object</option>
                            <option value="Count Records">Count Records</option>
                        </select>
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}', 'jdbcPost_form_', function(eid, status) {
                    if(status===1){
                    menuTree.updateNodeText($('#jdbcPost_form_name_' + eid).val());
                    }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>