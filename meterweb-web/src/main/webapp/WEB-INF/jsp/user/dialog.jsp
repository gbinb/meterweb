<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div id="dlg_user" class="easyui-window" buttons="#dlg_user_buttons"
     data-options="modal:true,closed:true,width:560,inline:true,border:'thin',cls:'c6'">
    <div style="padding: 20px 30px">
        <form id="form_user" method="post">
            <input id="form_user_id" type="hidden" name="id">
            <div class="form_item_height">
                <input class="easyui-textbox" id="form_user_username" name="username" style="width:80%"
                       data-options="label:'用户名：',labelWidth:100,required:true">
            </div>
            <div class="form_item_height">
                <input class="easyui-passwordbox" id="form_user_password" name="password" style="width:80%"
                       data-options="label:'密码：',labelWidth:100,required:true,validType:'minLength[6]'">
            </div>
            <div class="form_item_height">
                <input class="easyui-passwordbox" id="form_user_confirmPassword" style="width:80%"
                       data-options="label:'确认密码：',labelWidth:100,required:true" validType="equalTo['#form_user_password']">
            </div>
            <div class="form_item_height">
                <input class="easyui-textbox" name="name" style="width:80%"
                       data-options="label:'姓名：',labelWidth:100,required:true">
            </div>
            <div class="form_item_height">
                <input class="easyui-textbox" name="mail" style="width:80%"
                       data-options="label:'邮箱：',labelWidth:100,required:true,validType:'email'">
            </div>
            <div class="form_item_height">
                <input class="easyui-textbox" name="note" style="width:80%"
                       data-options="label:'备注：',labelWidth:100,required:false">
            </div>
        </form>
    </div>
</div>
<div id="dlg_user_buttons">
    <a href="javascript:void(0)" onclick="systemLib.saveUser()" class="btn btn-primary">
        <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M15.854 5.646a.5.5 0 010 .708l-7 7a.5.5 0 01-.708 0l-3.5-3.5a.5.5 0 11.708-.708L8.5 12.293l6.646-6.647a.5.5 0 01.708 0z" clip-rule="evenodd"></path>
        </svg>
        保存
    </a>
    <a href="javascript:void(0)" onclick="$('#dlg_user').dialog('close')" class="btn btn-secondary">
        <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M7.578 6.437a5 5 0 104.922.044l.5-.865a6 6 0 11-5.908-.053l.486.874z" clip-rule="evenodd"></path>
            <path fill-rule="evenodd" d="M9.5 10V3h1v7h-1z" clip-rule="evenodd"></path>
        </svg>
        取消
    </a>
</div>